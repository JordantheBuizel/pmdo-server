﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server
{
    public static class Extensions
    {
        public static int ToInt(this string str) {
            if (!string.IsNullOrEmpty(str) && int.TryParse(str, out int result)) {
                return result;
            } else
                return 0;
        }

        public static int ToInt(this string str, int defaultVal) {
            if (str != null && int.TryParse(str, out int result) == true) {
                return result;
            } else
                return defaultVal;
        }

        public static double ToDbl(this string str) {
            if (str != null && double.TryParse(str, out double result) == true) {
                return result;
            } else
                return 0;
        }

        public static double ToDbl(this string str, double defaultVal) {
            if (str != null && double.TryParse(str, out double result) == true) {
                return result;
            } else
                return defaultVal;
        }

        public static string ToIntString(this bool boolval) {
            if (boolval == true)
                return "1";
            else
                return "0";
        }

        public static bool IsNumeric(this string str) {
            return int.TryParse(str, out int result);
        }

        public static ulong ToUlng(this string str) {
            if (ulong.TryParse(str, out ulong result) == true) {
                return result;
            } else
                return 0;
        }

        /// <summary>
        /// Convert generic integer to bool
        /// </summary>
        /// <param name="num">Integer to convert</param>
        /// <returns>False if 0, otherwise true</returns>
        public static bool ToBool(this int num)
        {
            return (num == 0) ? false : true;
        }

        public static bool ToBool(this string str) {
            if (!string.IsNullOrEmpty(str)) {
                switch (str.ToLower()) {
                    case "true":
                        return true;
                    case "false":
                        return false;
                    case "1":
                        return true;
                    case "0":
                        return false;
                    case "yes":
                        return true;
                    case "no":
                        return false;
                    default:
                        return false;
                }
            } else {
                return false;
            }
        }

        /// <summary>
        /// Used for scripted methods
        /// </summary>
        /// <param name="obj">Scripted method to convert. Warning: Do not use for anything but scripted methods!</param>
        /// <returns></returns>
        public static bool ToBool(this object obj) {
            if (obj != null) {
                return (bool)obj;
            } else {
                return false;
            }
        }

        public static DateTime? ToDate(this string date) {
            if (DateTime.TryParse(date, out var tmpDate)) {
                return tmpDate;
            } else {
                return null;
            }
        }
    }
}

namespace System.Runtime.CompilerServices
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class ExtensionAttribute : Attribute
    {
    }
}

