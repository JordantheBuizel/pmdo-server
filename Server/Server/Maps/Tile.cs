﻿namespace Server.Maps
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Data = DataManager.Maps;

    public class Tile
    {
        Data.Tile tile;

        public Data.Tile RawTile {
            get { return tile; }
        }

        #region Constructors

        public Tile(Data.Tile tile)
        {
            this.tile = tile;
        }

        #endregion Constructors

        #region Properties

        public int Anim
        {
            get { return tile.Anim; }
            set { tile.Anim = value; }
        }

        public int AnimSet
        {
            get { return tile.AnimSet; }
            set { tile.AnimSet = value; }
        }

        public int Data1
        {
            get { return tile.Data1; }
            set { tile.Data1 = value; }
        }

        public int Data2
        {
            get { return tile.Data2; }
            set { tile.Data2 = value; }
        }

        public int Data3
        {
            get { return tile.Data3; }
            set { tile.Data3 = value; }
        }

        public bool DoorOpen
        {
            get; set;
        }

        public TickCount DoorTimer
        {
            get; set;
        }

        public int F2Anim
        {
            get { return tile.F2Anim; }
            set { tile.F2Anim = value; }
        }

        public int F2AnimSet
        {
            get { return tile.F2AnimSet; }
            set { tile.F2AnimSet = value; }
        }

        public int FAnim
        {
            get { return tile.FAnim; }
            set { tile.FAnim = value; }
        }

        public int FAnimSet
        {
            get { return tile.FAnimSet; }
            set {
                tile.FAnimSet = value;
            }
        }

        public int Fringe
        {
            get { return tile.Fringe; }
            set { tile.Fringe = value; }
        }

        public int Fringe2
        {
            get { return tile.Fringe2; }
            set { tile.Fringe2 = value; }
        }

        public int Fringe2Set
        {
            get { return tile.Fringe2Set; }
            set { tile.Fringe2Set = value; }
        }

        public int FringeSet
        {
            get { return tile.FringeSet; }
            set { tile.FringeSet = value; }
        }

        public int Ground
        {
            get { return tile.Ground; }
            set { tile.Ground = value; }
        }

        public int GroundSet
        {
            get { return tile.GroundSet; }
            set { tile.GroundSet = value; }
        }

        public int GroundAnim
        {
            get { return tile.GroundAnim; }
            set { tile.GroundAnim = value; }
        }

        public int GroundAnimSet
        {
            get { return tile.GroundAnimSet; }
            set { tile.GroundAnimSet = value; }
        }
        public int RDungeonMapValue
        {
            get { return tile.RDungeonMapValue; }
            set { tile.RDungeonMapValue = value; }
        }

        public int M2Anim
        {
            get { return tile.M2Anim; }
            set { tile.M2Anim = value; }
        }

        public int M2AnimSet
        {
            get { return tile.M2AnimSet; }
            set { tile.M2AnimSet = value; }
        }

        public int Mask
        {
            get { return tile.Mask; }
            set { tile.Mask = value; }
        }

        public int Mask2
        {
            get { return tile.Mask2; }
            set { tile.Mask2 = value; }
        }

        public int Mask2Set
        {
            get { return tile.Mask2Set; }
            set { tile.Mask2Set = value; }
        }

        public int MaskSet
        {
            get { return tile.MaskSet; }
            set { tile.MaskSet = value; }
        }

        public string String1
        {
            get { return tile.String1; }
            set { tile.String1 = value; }
        }

        public string String2
        {
            get { return tile.String2; }
            set { tile.String2 = value; }
        }

        public string String3
        {
            get { return tile.String3; }
            set { tile.String3 = value; }
        }

        public int AnimRotation
        {
            get { return tile.AnimRotation; }
            set { tile.AnimRotation = value; }
        }

        public bool AnimFlipped
        {
            get { return tile.AnimFlipped; }
            set { tile.AnimFlipped = value; }
        }

        public int F2AnimRotation
        {
            get { return tile.F2AnimRotation; }
            set { tile.F2AnimRotation = value; }
        }

        public bool F2AnimFlipped
        {
            get { return tile.F2AnimFlipped; }
            set { tile.F2AnimFlipped = value; }
        }

        public int FAnimRotation
        {
            get { return tile.FAnimRotation; }
            set { tile.FAnimRotation = value; }
        }

        public bool FAnimFlipped
        {
            get { return tile.FAnimFlipped; }
            set { tile.FAnimFlipped = value; }
        }

        public int FringeRotation
        {
            get { return tile.FringeRotation; }
            set { tile.FringeRotation = value; }
        }

        public bool FringeFlipped
        {
            get { return tile.FringeFlipped; }
            set { tile.FringeFlipped = value; }
        }

        public int Fringe2Rotation
        {
            get { return tile.Fringe2Rotation; }
            set { tile.Fringe2Rotation = value; }
        }

        public bool Fringe2Flipped
        {
            get { return tile.Fringe2Flipped; }
            set { tile.Fringe2Flipped = value; }
        }

        public int GroundRotation
        {
            get { return tile.GroundRotation; }
            set { tile.GroundRotation = value; }
        }

        public bool GroundFlipped
        {
            get { return tile.GroundFlipped; }
            set { tile.GroundFlipped = value; }
        }

        public int GroundAnimRotation
        {
            get { return tile.GroundAnimRotation; }
            set { tile.GroundAnimRotation = value; }
        }

        public bool GroundAnimFlipped
        {
            get { return tile.GroundAnimFlipped; }
            set { tile.GroundAnimFlipped = value; }
        }

        public int M2AnimRotation
        {
            get { return tile.M2AnimRotation; }
            set { tile.M2AnimRotation = value; }
        }

        public bool M2AnimFlipped
        {
            get { return tile.M2AnimFlipped; }
            set { tile.M2AnimFlipped = value; }
        }

        public int MaskRotation
        {
            get { return tile.MaskRotation; }
            set { tile.MaskRotation = value; }
        }

        public bool MaskFlipped
        {
            get { return tile.MaskFlipped; }
            set { tile.MaskFlipped = value; }
        }

        public int Mask2Rotation
        {
            get { return tile.Mask2Rotation; }
            set { tile.Mask2Rotation = value; }
        }

        public bool Mask2Flipped
        {
            get { return tile.Mask2Flipped; }
            set { tile.Mask2Flipped = value; }
        }

        public Enums.TileType Type
        {
            get { return (Enums.TileType)tile.Type; }
            set { tile.Type = (int)value; }
        }

        #endregion Properties
    }
}