﻿// This file is part of Mystery Dungeon eXtended.

// Copyright (C) 2015 Pikablu, MDX Contributors, PMU Staff

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Windows.Forms;

namespace Server
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            try {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

                if (File.Exists(Path.Combine(Application.StartupPath, "update.dat")))
                {
                    File.Delete(Path.Combine(Application.StartupPath, "update.dat"));
                }

                Application.ThreadException += new ThreadExceptionEventHandler(ThreadExceptionHandler);
                AppDomain.CurrentDomain.FirstChanceException += new EventHandler<System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs>(CurrentDomain_FirstChanceException);
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                ServerLoader.LoadComplete += new EventHandler(ServerLoader_LoadComplete);
                ServerLoader.LoadServer();
                Application.Run();
            } catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
        }

        private static void ThreadExceptionHandler(object sender, ThreadExceptionEventArgs e)
        {
            //MessageBox.Show(e.Exception.Message + "\n" + e.Exception.StackTrace + e.Exception.Source);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {

            //MessageBox.Show((e.ExceptionObject as Exception).Message + "\n" + (e.ExceptionObject as Exception).StackTrace + (e.ExceptionObject as Exception).Source);

            /*if (!(e.ExceptionObject as Exception).Message.StartsWith(@"Access to the path "))
            {
                Zayko.Dialogs.UnhandledExceptionDlg.UnhandledExceptionDlg exDia = new Zayko.Dialogs.UnhandledExceptionDlg.UnhandledExceptionDlg();
                exDia.ShowUnhandledExceptionDlg(e.ExceptionObject as Exception);
            }*/
        }

        static void CurrentDomain_FirstChanceException(object sender, FirstChanceExceptionEventArgs e) {

            //MessageBox.Show(e.Exception.Message + "\n" + e.Exception.StackTrace + e.Exception.Source);
            /*if (!e.Exception.Message.StartsWith(@"Access to the path "))
            {
                Zayko.Dialogs.UnhandledExceptionDlg.UnhandledExceptionDlg exDia = new Zayko.Dialogs.UnhandledExceptionDlg.UnhandledExceptionDlg();
                exDia.ShowUnhandledExceptionDlg(e.Exception);
            }*/
        }

        static void ServerLoader_LoadComplete(object sender, EventArgs e) {
            try {
                Application.Run(Globals.MainUI);
            } catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            //Globals.MainUI.Show();
        }
    }
}
