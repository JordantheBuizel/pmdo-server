﻿using Discord;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Report
{
    public class CSharpDewott
    {

        public DiscordClient client;

        private Discord.Server publicServer;

        private User jordan;

        private List<User> mods;

        public CSharpDewott()
        {

            this.client = new DiscordClient();
            
            Globals.DewottReporter = this;

            client.Log.Message += new EventHandler<LogMessageEventArgs>(async (a, message) =>
            {
                await Console.Out.WriteLineAsync($"[{message.Severity}] {message.Source} -> {message.Message}");
            });

            client.ExecuteAndWait(async () =>
            {
                await this.client.Connect("Mjc5MTQxOTE3NjU1MTcxMDcz.C32jeA.jCOs5yyt4iPOKi8AO9h0bvww5aI", TokenType.Bot);

                foreach (Discord.Server server in client.Servers)
                {
                    if (server.Name == "Pokémon Mystery Dungeon Online")
                    {
                        publicServer = server;

                        jordan = publicServer.GetUser(228019100008316948);

                        foreach (User user in publicServer.GetRole(280883851176181772).Members)
                        {
                            mods.Add(user);
                        }
                    }
                }

            });
        }

        public async Task SendReportToJordan(string text)
        {
            Channel x = await client.CreatePrivateChannel(228019100008316948);
            await x.SendMessage(text + "\n" + DateTime.Now.ToLongDateString() + " at " + DateTime.Now.ToLocalTime().ToLongTimeString());
        }

        public async Task SendReportScreenToJordan(Bitmap bmp)
        {
            
            using (MemoryStream ms = new MemoryStream())
            {
                bmp.Save(ms, ImageFormat.Png);
                Channel x = await client.CreatePrivateChannel(228019100008316948);
                await x.SendFile("screenimg.png", ms);
            }
            bmp.Dispose();
        }

        public void SendReportToMods(string text)
        {
            foreach (User user in mods)
            {
                user.PrivateChannel.SendMessage(text + "\n" + DateTime.Now.ToLongDateString());
            }
        }

    }
}
