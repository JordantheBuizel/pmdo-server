﻿extern alias MySQL;
using Server.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySQL::MySql.Data.MySqlClient;
using Server.Network;

namespace Server.Items
{
    public class MoneyManager
    {
        string cso = @"server=" + Settings.DatabaseIP + @";userid=" + Settings.DatabaseUser + @";password=" + Settings.DatabasePassword + @";database=" + "pmu_players";

        string cs
        {
            get
            {
                if (Settings.DatabasePassword == "")
                {
                    cso = @"server=" + Settings.DatabaseIP + @";userid=" + Settings.DatabaseUser + @";database=" + "pmu_players";
                    return cso;
                }
                else
                {
                    return cso;
                }
            }
        }

        private string CharID;
        private bool invalid = false;

        public MoneyManager(Client client)
        {
            CharID = client.Player.CharID;
        }

        public MoneyManager(Players.Player player)
        {
            CharID = player.CharID;
        }

        public MoneyManager(string CharID)
        {

            using (DatabaseConnection conn = new DatabaseConnection(DatabaseID.Players))
            {
                string name = Players.PlayerManager.RetrieveCharacterName(conn, CharID);
                if (string.IsNullOrEmpty(name))
                {
                    invalid = true;
                }
            }

            this.CharID = CharID;
        }
        
        public void TakeMoney(int amount)
        {
            bool rowExists = false;

            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            string query = "SELECT Money FROM `money` WHERE CharID = '" + CharID + "'";
            MySqlCommand com = new MySqlCommand(query, conn);
            MySqlDataReader reader = com.ExecuteReader();
            int currentamount = 0;

            while (reader.Read())
            {
                currentamount = reader.GetInt32("Money");
            }

            reader.Close();
            string command = "";

            if (currentamount == 0)
            {
                try
                {
                    command = @"INSERT INTO `money` (`CharID`, `Money`) VALUES ('" + CharID + @"', " + 0 + @")";
                    com = new MySqlCommand(command, conn);
                    com.ExecuteScalar();
                    conn.Close();
                    return;
                }
                catch
                {
                    currentamount = 0;
                    rowExists = true;
                }
            }

            if (currentamount != 0 || rowExists)
            {
                int newamount = currentamount - amount;
                if (newamount < 0)
                    newamount = 0;
                command = @"REPLACE INTO `money` (`CharID`, `Money`) VALUES ('" + CharID + @"', " + newamount + @")";
                com = new MySqlCommand(command, conn);
                com.ExecuteScalar();
                conn.Close();
            }

            
        }

        public void GiveMoney(int amount)
        {
            bool rowExists = false;

            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            string query = "SELECT Money FROM `money` WHERE CharID = '" + CharID + "'";
            MySqlCommand com = new MySqlCommand(query, conn);
            MySqlDataReader reader = com.ExecuteReader();
            int currentamount = 0;
            while (reader.Read())
            {
                currentamount = reader.GetInt32("Money");
            }
            reader.Close();
            string command = "";

            if (currentamount == 0)
            {
                try
                {
                    command = @"INSERT INTO `money` (`CharID`, `Money`) VALUES ('" + CharID + @"', " + amount + @")";
                    com = new MySqlCommand(command, conn);
                    com.ExecuteScalar();
                    conn.Close();
                    return;
                }
                catch
                {
                    currentamount = 0;
                    rowExists = true;
                }
            }

            if (currentamount != 0 || rowExists)
            {
                int newamount = currentamount + amount;
                if (newamount > 999999999)
                {
                    DepositMoneyToBank((newamount - 999999999));
                    using (DatabaseConnection dbconn = new DatabaseConnection(DatabaseID.Players))
                    {
                        Messenger.PlayerMsg(ClientManager.FindClientFromCharID(CharID), "You can't carry anymore Poké in your bag! " + (newamount - 999999999) + " Pokés were sent to the bank.", System.Drawing.Color.Blue);
                    }
                    newamount = 999999999;
                }

                command = @"REPLACE INTO `money` (`CharID`, `Money`) VALUES ('" + CharID + @"', " + newamount + @")";
                com = new MySqlCommand(command, conn);
                com.ExecuteScalar();
                conn.Close();
            }
        }

        public int GetMoney()
        {
            
            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            string query = "SELECT Money FROM `money` WHERE CharID = '" + CharID + "'";
            MySqlCommand com = new MySqlCommand(query, conn);
            MySqlDataReader reader = com.ExecuteReader();
            int currentamount = 0;

            while (reader.Read())
            {
                currentamount = reader.GetInt32("Money");
            }
            reader.Close();
            string command = "";

            if (currentamount == 0)
            {
                try
                {
                    command = @"INSERT INTO `money` (`CharID`, `Money`) VALUES ('" + CharID + @"', " + 0 + @")";
                    com = new MySqlCommand(command, conn);
                    com.ExecuteScalar();
                    currentamount = 0;
                }
                catch
                {
                    currentamount = 0;
                }
            }

            conn.Close();

            return currentamount;
        }

        public void DepositMoneyToBank(long amount)
        {
            bool rowExists = false;

            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            string query = "SELECT Money FROM `bank_money` WHERE CharID = '" + CharID + "'";
            MySqlCommand com = new MySqlCommand(query, conn);
            MySqlDataReader reader = com.ExecuteReader();
            long currentamount = 0;

            while (reader.Read())
            {
                currentamount = reader.GetInt64("Money");
            }
            reader.Close();
            string command = "";

            if (currentamount == 0)
            {
                try
                {
                    command = @"INSERT INTO `bank_money` (`CharID`, `Money`) VALUES ('" + CharID + @"', " + amount + @")";
                    com = new MySqlCommand(command, conn);
                    com.ExecuteScalar();
                    conn.Close();
                    return;
                }
                catch
                {
                    currentamount = 0;
                    rowExists = true;
                }
            }

            if (currentamount != 0 || rowExists)
            {
                ulong testamount = (ulong)(currentamount + amount);
                if (testamount > long.MaxValue)
                {
                    command = @"REPLACE INTO `bank_money` (`CharID`, `Money`) VALUES ('" + CharID + @"', " + long.MaxValue + @")";
                    Messenger.AdminMsg("[Staff] " + ClientManager.FindClientFromCharID(CharID).Player.Name + " is probably hacking; they have " + long.MaxValue + " Poké in their bank account.", System.Drawing.Color.Red);
                    return;
                }
                long newamount = currentamount + amount;
                command = @"REPLACE INTO `bank_money` (`CharID`, `Money`) VALUES ('" + CharID + @"', " + newamount + @")";
                com = new MySqlCommand(command, conn);
                com.ExecuteScalar();
                conn.Close();
                if (amount > int.MaxValue)
                {
                    long instances = amount / int.MaxValue;
                }
            }

            
        }

        public void WithdrawMoneyFromBank(int amount)
        {
            
            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            string query = "SELECT Money FROM `bank_money` WHERE CharID = '" + CharID + "'";
            MySqlCommand com = new MySqlCommand(query, conn);
            MySqlDataReader reader = com.ExecuteReader();
            long currentamount = 0;

            while (reader.Read())
            {
                currentamount = reader.GetInt64("Money");
            }
            reader.Close();
            string command = "";

            if (currentamount == 0)
            {
                Messenger.PlayerMsg(ClientManager.FindClientFromCharID(CharID), "You don't have " + amount + " Pokés in your bank!", System.Drawing.Color.Red);
            }

            else if (currentamount != 0)
            {
                long newamount = currentamount - (long)amount;
                if (newamount < 0)
                {
                    Messenger.PlayerMsg(ClientManager.FindClientFromCharID(CharID), "You don't have " + amount + " Pokés in your bank!", System.Drawing.Color.Red);
                    return;
                }

                command = @"REPLACE INTO `bank_money` (`CharID`, `Money`) VALUES ('" + CharID + @"', " + newamount + @")";
                com = new MySqlCommand(command, conn);
                com.ExecuteScalar();
                conn.Close();
                GiveMoney(amount);
            }

            
        }
        
        public long GetBankMoney()
        {
            
            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            string query = "SELECT Money FROM `bank_money` WHERE CharID = '" + CharID + "'";
            MySqlCommand com = new MySqlCommand(query, conn);
            MySqlDataReader reader = com.ExecuteReader();
            long currentamount = 0;

            while (reader.Read())
            {
                currentamount = reader.GetInt64("Money");
            }
            reader.Close();
            string command = "";

            if (currentamount == 0)
            {
                try
                {
                    command = @"INSERT INTO `bank_money` (`CharID`, `Money`) VALUES ('" + CharID + @"', " + 0 + @")";
                    com = new MySqlCommand(command, conn);
                    com.ExecuteScalar();
                }
                catch
                {
                    currentamount = 0;
                }
            }

            conn.Close();

            return currentamount;
        }

    }
}
