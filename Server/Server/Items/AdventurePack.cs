﻿extern alias MySQL;

using MySQL::MySql.Data.MySqlClient;
using Server.Database;
using Server.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Players
{
    public class AdventurePack
    {

        public enum Abilities
        {
            RockSmash,
            RockClimb,
            Surf,
            Fly,
            Dive
        }

        public bool canRockSmash
        {
            get;
            private set;
        }

        public bool canRockClimb
        {
            get;
            private set;
        }

        public bool canSurf
        {
            get;
            private set;
        }

        public bool canFly
        {
            get;
            private set;
        }

        public bool canDive
        {
            get;
            private set;
        }

        string cso = @"server=" + Settings.DatabaseIP + @";userid=" + Settings.DatabaseUser + @";password=" + Settings.DatabasePassword + @";database=" + "pmu_players";

        string cs
        {
            get
            {
                if (Settings.DatabasePassword == "")
                {
                    cso = @"server=" + Settings.DatabaseIP + @";userid=" + Settings.DatabaseUser + @";database=" + "pmu_players";
                    return cso;
                }
                else
                {
                    return cso;
                }
            }
        }

        private string CharID;
        private bool invalid = false;

        public AdventurePack(Client client)
        {
            CharID = client.Player.CharID;
            LoadAvailibleTools();
        }

        public AdventurePack(Players.Player player)
        {
            CharID = player.CharID;
            LoadAvailibleTools();
        }

        public AdventurePack(string CharID)
        {

            using (DatabaseConnection conn = new DatabaseConnection(DatabaseID.Players))
            {
                string name = Players.PlayerManager.RetrieveCharacterName(conn, CharID);
                if (string.IsNullOrEmpty(name))
                {
                    invalid = true;
                }
            }

            this.CharID = CharID;
            LoadAvailibleTools();
        }

        public void LoadAvailibleTools()
        {
            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            List<string> queries = new List<string>()
            {
                "MM", "CL", "AF", "CW", "AB"
            };

            foreach (string toolName in queries)
            {
                string query = "SELECT " + toolName + " FROM `aquired_tools` WHERE CharID = '" + CharID + "'";
                MySqlCommand com = new MySqlCommand(query, conn);
                MySqlDataReader reader = com.ExecuteReader();
                int currentamount = 0;

                while (reader.Read())
                {
                    currentamount = reader.GetInt32(toolName);
                }

                reader.Close();
                string command = "";

                if (currentamount == 0)
                {
                    try
                    {
                        command = @"INSERT INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 0 + @")";
                        com = new MySqlCommand(command, conn);
                        com.ExecuteScalar();
                    }
                    catch
                    {
                        currentamount = 0;
                    }

                    switch (toolName)
                    {
                        case "MM":
                            {
                                canRockSmash = false;
                            }
                            break;
                        case "CL":
                            {
                                canRockClimb = false;
                            }
                            break;
                        case "AF":
                            {
                                canSurf = false;
                            }
                            break;
                        case "CW":
                            {
                                canFly = false;
                            }
                            break;
                        case "AB":
                            {
                                canDive = false;
                            }
                            break;
                    }

                }

                else
                {
                    switch (toolName)
                    {
                        case "MM":
                            {
                                canRockSmash = true;
                            }
                            break;
                        case "CL":
                            {
                                canRockClimb = true;
                            }
                            break;
                        case "AF":
                            {
                                canSurf = true;
                            }
                            break;
                        case "CW":
                            {
                                canFly = true;
                            }
                            break;
                        case "AB":
                            {
                                canDive = true;
                            }
                            break;
                    }
                }
            }

                conn.Close();

            }

        public void SaveAbilities()
        {
            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            List<string> queries = new List<string>()
            {
                "MM", "CL", "AF", "CW", "AB"
            };

            foreach (string toolName in queries)
            {
                string query = "SELECT " + toolName + " FROM `aquired_tools` WHERE CharID = '" + CharID + "'";
                MySqlCommand com = new MySqlCommand(query, conn);
                MySqlDataReader reader = com.ExecuteReader();
                int currentamount = 0;

                while (reader.Read())
                {
                    currentamount = reader.GetInt32(toolName);
                }

                reader.Close();
                string command = "";

                if (currentamount == 0)
                {
                    try
                    {
                        command = @"INSERT INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 0 + @")";
                        com = new MySqlCommand(command, conn);
                        com.ExecuteScalar();
                    }
                    catch
                    {
                        currentamount = 0;
                    }

                }

                switch (toolName)
                {
                    case "MM":
                        {
                            if (canRockSmash)
                            {
                                command = @"REPLACE INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 1 + @")";
                            }
                            else
                            {
                                command = @"REPLACE INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 0 + @")";
                            }
                        }
                        break;
                    case "CL":
                        {
                            if (canRockClimb)
                            {
                                command = @"REPLACE INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 1 + @")";
                            }
                            else
                            {
                                command = @"REPLACE INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 0 + @")";
                            }
                        }
                        break;
                    case "AF":
                        {
                            if (canSurf)
                            {
                                command = @"REPLACE INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 1 + @")";
                            }
                            else
                            {
                                command = @"REPLACE INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 0 + @")";
                            }
                        }
                        break;
                    case "CW":
                        {
                            if (canFly)
                            {
                                command = @"REPLACE INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 1 + @")";
                            }
                            else
                            {
                                command = @"REPLACE INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 0 + @")";
                            }
                        }
                        break;
                    case "AB":
                        {
                            if (canDive)
                            {
                                command = @"REPLACE INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 1 + @")";
                            }
                            else
                            {
                                command = @"REPLACE INTO `aquired_tools` (`CharID`, `" + toolName + "`) VALUES ('" + CharID + @"', " + 0 + @")";
                            }
                        }
                        break;
                }

                com = new MySqlCommand(command, conn);
                com.ExecuteScalar();
            }

            conn.Close();

        }

        /// <summary>
        /// Adds an ability to player.
        /// </summary>
        /// <param name="ability">Ability to add</param>
        public void AddAbility(Abilities ability)
        {
            switch (ability)
            {
                case Abilities.RockClimb:
                    {
                        canRockClimb = true;
                    }
                    break;
                case Abilities.RockSmash:
                    {
                        canRockSmash = true;
                    }
                    break;
                case Abilities.Surf:
                    {
                        canSurf = true;
                    }
                    break;
                case Abilities.Fly:
                    {
                        canFly = true;
                    }
                    break;
                case Abilities.Dive:
                    {
                        canDive = true;
                    }
                    break;
            }

            SaveAbilities();

        }

        public bool HasAbility(Abilities ability)
        {
            switch (ability)
            {
                case Abilities.RockClimb:
                        return canRockClimb;
                case Abilities.RockSmash:
                        return canRockSmash;
                case Abilities.Surf:
                        return canSurf;
                case Abilities.Fly:
                        return canFly;
                case Abilities.Dive:
                        return canDive;
                default:
                    return false;
            }
        }

    }
}
