﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Updater
{
    public partial class UpdaterMain : Form
    {
        public UpdaterMain()
        {
            InitializeComponent();

            TopMost = true;

            backgroundWorker1.RunWorkerAsync();
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Stopwatch sw = new Stopwatch(); // sw cotructor
            sw.Start(); // starts the stopwatch
            for (int i = 0; ; i++)
            {
                if (i % 10 == 0) // if in 100000th iteration (could be any other large number
                                     // depending on how often you want the time to be checked) 
                {
                    sw.Stop(); // stop the time measurement
                    if (sw.ElapsedMilliseconds > 5000) // check if desired period of time has elapsed
                    {
                        break; // if more than 5000 milliseconds have passed, stop looping and return
                               // to the existing code
                    }
                    else
                    {
                        sw.Start(); // if less than 5000 milliseconds have elapsed, continue looping
                                    // and resume time measurement
                    }
                }
            }

            Process.Start(Path.Combine(Application.StartupPath, "Server.exe"));
            Close();
        }

        private void UpdatePMDO()
        {
            int bytesRead = 0;
            byte[] buffer = new byte[2048];
            try {
                FtpWebRequest request = CreateFtpWebRequest("ftp://localhost/ServerUpdate.zip", "anonymous", "", true);
                request.Method = WebRequestMethods.Ftp.DownloadFile;

                Stream reader = request.GetResponse().GetResponseStream();
                FileStream fileStream = new FileStream(Path.Combine(Application.StartupPath, "Data", "ServerUpdate.zip"), FileMode.Create);

                while (true)
                {
                    bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                        break;

                    fileStream.Write(buffer, 0, bytesRead);
                }
                fileStream.Dispose();
                fileStream.Close();
            }
            catch(Exception ex)
            {
                //Assume update server is down.
                Process.Start(Path.Combine(Application.StartupPath, "Server.exe"));
                Close();
            }

            System.IO.Compression.ZipFile.ExtractToDirectory(Path.Combine(Application.StartupPath, "Data", "ServerUpdate.zip"), Path.Combine(Application.StartupPath, "Data"));

            CopyAll(new DirectoryInfo(Path.Combine(Application.StartupPath, "Data", "Release")), new DirectoryInfo(Application.StartupPath));

            Directory.Delete(Path.Combine(Application.StartupPath, "Data", "Release"), true);
            File.Delete(Path.Combine(Application.StartupPath, "Data", "ServerUpdate.zip"));

        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            if (source.FullName.ToLower() == target.FullName.ToLower())
            {
                return;
            }

            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }

            // Copy each file into it's new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        private FtpWebRequest CreateFtpWebRequest(string ftpDirectoryPath, string userName, string password, bool keepAlive = false)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ftpDirectoryPath));

            //Set proxy to null. Under current configuration if this option is not set then the proxy that is used will get an html response from the web content gateway (firewall monitoring system)
            request.Proxy = null;

            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = keepAlive;

            request.Credentials = new NetworkCredential(userName, password);

            return request;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(UpdatePMDO));
            t.SetApartmentState(System.Threading.ApartmentState.STA); //Set the thread to STA
            t.Start();
        }
    }
}
