using System;
using Zayko.Dialogs.UnhandledExceptionDlg;

namespace UnhandledExceptionExample
{
    class Program
    {
        static void Main()
        {
            // Create new instance of UnhandledExceptionDlg:
            UnhandledExceptionDlg exDlg = new UnhandledExceptionDlg();

            // Uncheck "Restart App" check box by default:
            exDlg.RestartApp = false;

            // Add handling of OnShowErrorReport.
            // If you skip this then link to report details won't be showing.
            exDlg.OnShowErrorReport += delegate(object sender, SendExceptionClickEventArgs ar)
            {
                System.Windows.Forms.MessageBox.Show("Handle OnShowErrorReport event to show what you are going to send.\n" +
                    "For example:\n" + ar.UnhandledException.Message + "\n" + ar.UnhandledException.StackTrace + 
                    "\n" + (ar.RestartApp ? "This App will be restarted." : "This App will be terminated!"));
            };

            // Implement your sending protocol here. You can use any information from System.Exception
            exDlg.OnSendExceptionClick += delegate(object sender, SendExceptionClickEventArgs ar)
            {
                // User clicked on "Send Error Report" button:
                if(ar.SendExceptionDetails)
                    System.Windows.Forms.MessageBox.Show(String.Format("Implement your communication part here " +
                        "(do HTTP POST or send e-mail, for example).\nExample:\nError Message: {0}\r" +
                        "Stack Trace:\n{1}",
                        ar.UnhandledException.Message, ar.UnhandledException.StackTrace));
                
                // User wants to restart the App:
                if(ar.RestartApp)
                {
                    Console.WriteLine("The App will be restarted...");
                    System.Diagnostics.Process.Start(System.Windows.Forms.Application.ExecutablePath);
                }
            };

            Console.Write("Do you want to generate exception?\n\t" + 
                "1 - Main Thread Exception,\n\t2 - UI Thread Exception,\n\t" +
                "3 - Separate Thread Exception,\n\t4 - Unmanaged Exception\n\t" +
                "0 - don't generate any Exceptions: ");
            ConsoleKeyInfo info = Console.ReadKey(false);
            Console.WriteLine();
            switch(info.KeyChar)
            {
                // Main Thread Exception:
                case '1':
                    Console.WriteLine("\nCreating an Exception...");
                    throw new Exception("Custom Unhandled Exception!");
                case '2':
                    // UI Exception:
                    Console.WriteLine("\nCreating Form for UI Exception Demo...");
                    exceptionForm ef = new exceptionForm();
                    try
                    {
                        ef.ShowDialog();
                    }
                    finally
                    {
                        ef.Dispose();
                    }
                    break;
                case '3':
                    // Separate Thread Exception:
                    Console.WriteLine("\nCreating an Exception Thread...");
                    System.Threading.Thread exceptionThread = new System.Threading.Thread(new System.Threading.ThreadStart(CustomThreadException));
                    exceptionThread.Start();
                    break;
                case '4':
                    byte b = CustomUnmanagedException();
                    break;
                default:
                    // No Exception:
                    Console.WriteLine("\nNo Exception will be generated.");
                    break;
            }

            Console.WriteLine("You can see this message only in case you didn't generate the exception! Press any key to close the Demo.");
            Console.ReadKey();
        }

        static void CustomThreadException()
        {
            throw new ApplicationException("\nSeparate Thread Exception raised!");
        }

        static unsafe byte CustomUnmanagedException()
        {
            byte* buf = null;
            return buf[0];
        }
    }
}
