using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UnhandledExceptionExample
{
    public partial class exceptionForm: Form
    {
        public exceptionForm()
        {
            InitializeComponent();
        }

        private void buttonException_Click(object sender, EventArgs e)
        {
            throw new ApplicationException("\nUI Thread Exception raised!");
        }

        
    }
}