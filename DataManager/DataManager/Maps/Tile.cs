﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataManager.Maps
{
    public class Tile
    {
        #region Properties

        public int Anim {
            get;
            set;
        }

        public int AnimSet {
            get;
            set;
        }

        public int Data1 {
            get;
            set;
        }

        public int Data2 {
            get;
            set;
        }

        public int Data3 {
            get;
            set;
        }

        public bool DoorOpen {
            get;
            set;
        }

        public int F2Anim {
            get;
            set;
        }

        public int F2AnimSet {
            get;
            set;
        }

        public int FAnim {
            get;
            set;
        }

        public int FAnimSet {
            get;
            set;
        }

        public int Fringe {
            get;
            set;
        }

        public int Fringe2 {
            get;
            set;
        }

        public int Fringe2Set {
            get;
            set;
        }

        public int FringeSet {
            get;
            set;
        }

        public int Ground {
            get;
            set;
        }

        public int GroundSet {
            get;
            set;
        }

        public int GroundAnim {
            get;
            set;
        }

        public int GroundAnimSet {
            get;
            set;
        }
        public int RDungeonMapValue {
            get;
            set;
        }

        public int M2Anim {
            get;
            set;
        }

        public int M2AnimSet {
            get;
            set;
        }

        public int Mask {
            get;
            set;
        }

        public int Mask2 {
            get;
            set;
        }

        public int Mask2Set {
            get;
            set;
        }

        public int MaskSet {
            get;
            set;
        }

        public string String1 {
            get;
            set;
        }

        public string String2 {
            get;
            set;
        }

        public string String3 {
            get;
            set;
        }

        public int Type {
            get;
            set;
        }

        public int AnimRotation
        {
            get;
            set;
        }

        public bool AnimFlipped
        {
            get;
            set;
        }

        public int F2AnimRotation
        {
            get;
            set;
        }

        public bool F2AnimFlipped
        {
            get;
            set;
        }

        public int FAnimRotation
        {
            get;
            set;
        }

        public bool FAnimFlipped
        {
            get;
            set;
        }

        public int FringeRotation
        {
            get;
            set;
        }

        public bool FringeFlipped
        {
            get;
            set;
        }

        public int Fringe2Rotation
        {
            get;
            set;
        }

        public bool Fringe2Flipped
        {
            get;
            set;
        }

        public int GroundRotation
        {
            get;
            set;
        }

        public bool GroundFlipped
        {
            get;
            set;
        }

        public int GroundAnimRotation
        {
            get;
            set;
        }

        public bool GroundAnimFlipped
        {
            get;
            set;
        }

        public int M2AnimRotation
        {
            get;
            set;
        }

        public bool M2AnimFlipped
        {
            get;
            set;
        }

        public int MaskRotation
        {
            get;
            set;
        }

        public bool MaskFlipped
        {
            get;
            set;
        }

        public int Mask2Rotation
        {
            get;
            set;
        }

        public bool Mask2Flipped
        {
            get;
            set;
        }

        #endregion Properties
        // "GroundRotation", "GroundFlipped", "GAnimRotation", "GAnimFlipped", "MaskRotation", "MaskFlipped", "MAnimRotation", "MAnimFlipped", "M2Rotation", "M2Flipped", "M2AnimRotation", "M2AnimFlipped", "FringeRotation", "FringFlipped", "FAnimRotation", "FAnimFlipped", "F2Rotation", "F2Flipped", "F2AnimRotation", "F2AnimFlipped"
    }
}
