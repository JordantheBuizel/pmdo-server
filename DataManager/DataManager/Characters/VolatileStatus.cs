﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataManager.Characters
{
    [Serializable]
    public class VolatileStatus
    {
        public string Name { get; set; }
        public int Emoticon { get; set; }
        public int Counter { get; set; }
        public string Tag { get; set; }
    }
}
