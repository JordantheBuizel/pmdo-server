@echo off
echo Loading database...
"C:\Program Files\MySQL\MySQL Server 5.6\bin\mysql.exe" -u root -p1234 < "pmu_schemas.sql"
echo Loading data table into database...
"C:\Program Files\MySQL\MySQL Server 5.6\bin\mysql.exe" -u root -p1234 pmu_data < "pmu_data.sql"
echo Table Loaded!
echo Loading player table into database...
"C:\Program Files\MySQL\MySQL Server 5.6\bin\mysql.exe" -u root -p1234 pmu_players < "pmu_players.sql"
echo Table Loaded!
Pause