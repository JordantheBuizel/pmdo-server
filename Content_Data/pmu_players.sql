-- MySQL dump 10.13  Distrib 5.6.13, for Win64 (x86_64)
--
-- Host: localhost    Database: pmu_players
-- ------------------------------------------------------
-- Server version	5.6.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `AccountName` varchar(255) NOT NULL,
  `Password` text,
  `Email` text,
  PRIMARY KEY (`AccountName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank` (
  `CharID` varchar(255) NOT NULL,
  `ItemSlot` int(11) NOT NULL,
  `ItemNum` int(11) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  `Sticky` tinyint(1) DEFAULT NULL,
  `Tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`ItemSlot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bans`
--

DROP TABLE IF EXISTS `bans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bans` (
  `BannedPlayerID` varchar(255) NOT NULL,
  `BannedPlayerAccount` text,
  `BannedPlayerIP` varchar(255) DEFAULT NULL,
  `BannedPlayerMac` varchar(255) DEFAULT NULL,
  `BannerPlayerID` varchar(255) DEFAULT NULL,
  `BannerPlayerIP` varchar(255) DEFAULT NULL,
  `BannedDate` varchar(255) DEFAULT NULL,
  `UnbanDate` varchar(255) DEFAULT NULL,
  `BanType` int(11) DEFAULT NULL,
  PRIMARY KEY (`BannedPlayerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `character_statistics`
--

DROP TABLE IF EXISTS `character_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_statistics` (
  `CharID` varchar(255) NOT NULL,
  `TotalPlayTime` varchar(255) DEFAULT NULL,
  `LastIPAddressUsed` varchar(255) DEFAULT NULL,
  `LastMacAddressUsed` varchar(255) DEFAULT NULL,
  `LastLogin` varchar(255) DEFAULT NULL,
  `LastLogout` varchar(255) DEFAULT NULL,
  `LastPlayTime` varchar(255) DEFAULT NULL,
  `LastOS` varchar(255) DEFAULT NULL,
  `LastDotNetVersion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CharID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `characteristics`
--

DROP TABLE IF EXISTS `characteristics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `characteristics` (
  `CharID` varchar(255) NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Access` tinyint(4) DEFAULT '0',
  `ActiveSlot` int(11) DEFAULT '0',
  `PK` tinyint(1) DEFAULT '0',
  `Solid` tinyint(1) DEFAULT '0',
  `Status` varchar(20) DEFAULT NULL,
  `Veteran` tinyint(1) DEFAULT '0',
  `InTempMode` tinyint(1) DEFAULT '0',
  `Dead` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`CharID`),
  KEY `Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `characters`
--

DROP TABLE IF EXISTS `characters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `characters` (
  `AccountName` varchar(255) NOT NULL,
  `Slot` int(11) NOT NULL,
  `CharID` varchar(255) NOT NULL,
  PRIMARY KEY (`AccountName`,`Slot`,`CharID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dungeons`
--

DROP TABLE IF EXISTS `dungeons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dungeons` (
  `CharID` varchar(255) NOT NULL,
  `DungeonID` int(11) NOT NULL,
  `CompletionCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`DungeonID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `expkit`
--

DROP TABLE IF EXISTS `expkit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expkit` (
  `CharID` varchar(255) NOT NULL,
  `AvailableModules` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CharID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `CharID` varchar(255) NOT NULL,
  `FriendListSlot` int(11) NOT NULL,
  `FriendName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`FriendListSlot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `guild`
--

DROP TABLE IF EXISTS `guild`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild` (
  `CharID` varchar(255) NOT NULL,
  `GuildName` varchar(255) DEFAULT NULL,
  `GuildAccess` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`CharID`),
  KEY `GuildName` (`GuildName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory` (
  `CharID` varchar(255) NOT NULL,
  `ItemSlot` int(11) NOT NULL,
  `ItemNum` int(11) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  `Sticky` tinyint(1) DEFAULT NULL,
  `Tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`ItemSlot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `CharID` varchar(255) NOT NULL,
  `MaxInv` int(11) DEFAULT NULL,
  `MaxBank` int(11) DEFAULT NULL,
  PRIMARY KEY (`CharID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `job_list`
--

DROP TABLE IF EXISTS `job_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_list` (
  `CharID` varchar(255) NOT NULL,
  `JobListSlot` int(11) NOT NULL,
  `Accepted` int(11) DEFAULT NULL,
  `SendsRemaining` int(11) DEFAULT NULL,
  `ClientIndex` int(11) DEFAULT NULL,
  `TargetIndex` int(11) DEFAULT NULL,
  `RewardIndex` int(11) DEFAULT NULL,
  `MissionType` int(11) DEFAULT NULL,
  `Data1` int(11) DEFAULT NULL,
  `Data2` int(11) DEFAULT NULL,
  `DungeonIndex` int(11) DEFAULT NULL,
  `Goal` int(11) DEFAULT NULL,
  `RDungeon` tinyint(1) DEFAULT NULL,
  `StartScript` int(11) DEFAULT NULL,
  `WinScript` int(11) DEFAULT NULL,
  `LoseScript` int(11) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`JobListSlot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `CharID` varchar(255) NOT NULL,
  `Map` varchar(255) DEFAULT NULL,
  `X` int(11) DEFAULT '0',
  `Y` int(11) DEFAULT '0',
  `Direction` int(11) DEFAULT NULL,
  PRIMARY KEY (`CharID`),
  KEY `CharID` (`CharID`),
  KEY `Map` (`Map`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `map_load_trigger_event`
--

DROP TABLE IF EXISTS `map_load_trigger_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_load_trigger_event` (
  `CharID` varchar(255) NOT NULL,
  `ID` varchar(255) NOT NULL,
  `MapID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mission_board_missions`
--

DROP TABLE IF EXISTS `mission_board_missions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mission_board_missions` (
  `CharID` varchar(255) NOT NULL,
  `Slot` int(11) NOT NULL,
  `ClientIndex` int(11) DEFAULT NULL,
  `TargetIndex` int(11) DEFAULT NULL,
  `RewardIndex` int(11) DEFAULT NULL,
  `MissionType` tinyint(1) DEFAULT NULL,
  `Data1` int(11) DEFAULT NULL,
  `Data2` int(11) DEFAULT NULL,
  `DungeonIndex` int(11) DEFAULT NULL,
  `Goal` int(11) DEFAULT NULL,
  `RDungeon` tinyint(1) DEFAULT NULL,
  `StartScript` int(11) DEFAULT NULL,
  `WinScript` int(11) DEFAULT NULL,
  `LoseScript` int(11) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`Slot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `missions`
--

DROP TABLE IF EXISTS `missions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `missions` (
  `CharID` varchar(255) NOT NULL,
  `MissionExp` int(10) unsigned DEFAULT NULL,
  `LastGenTime` int(11) DEFAULT NULL,
  `Completions` int(11) DEFAULT NULL,
  PRIMARY KEY (`CharID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parties`
--

DROP TABLE IF EXISTS `parties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parties` (
  `PartyID` char(255) NOT NULL DEFAULT '',
  `PartySlot` int(11) NOT NULL DEFAULT '0',
  `CharID` char(255) DEFAULT NULL,
  PRIMARY KEY (`PartyID`,`PartySlot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recruit_data`
--

DROP TABLE IF EXISTS `recruit_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recruit_data` (
  `CharID` varchar(255) NOT NULL,
  `RecruitIndex` int(11) NOT NULL,
  `UsingTempStats` tinyint(1) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Species` int(11) DEFAULT NULL,
  `Nickname` tinyint(1) DEFAULT NULL,
  `NpcBase` int(11) DEFAULT NULL,
  `Sex` tinyint(4) DEFAULT NULL,
  `Shiny` int(11) NOT NULL DEFAULT '0',
  `Form` int(11) NOT NULL DEFAULT '0',
  `HeldItemSlot` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Experience` bigint(20) unsigned DEFAULT NULL,
  `HP` int(11) DEFAULT NULL,
  `StatusAilment` tinyint(4) DEFAULT NULL,
  `StatusAilmentCounter` int(11) DEFAULT NULL,
  `IQ` int(11) DEFAULT NULL,
  `Belly` int(11) DEFAULT NULL,
  `MaxBelly` int(11) DEFAULT NULL,
  `AttackBonus` int(11) DEFAULT NULL,
  `DefenseBonus` int(11) DEFAULT NULL,
  `SpeedBonus` int(11) DEFAULT NULL,
  `SpecialAttackBonus` int(11) DEFAULT NULL,
  `SpecialDefenseBonus` int(11) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`RecruitIndex`,`UsingTempStats`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recruit_list`
--

DROP TABLE IF EXISTS `recruit_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recruit_list` (
  `CharID` varchar(255) NOT NULL,
  `PokemonID` int(11) NOT NULL,
  `Status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`CharID`,`PokemonID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recruit_moves`
--

DROP TABLE IF EXISTS `recruit_moves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recruit_moves` (
  `CharID` varchar(255) NOT NULL,
  `RecruitIndex` int(11) NOT NULL,
  `UsingTempStats` tinyint(1) NOT NULL,
  `MoveSlot` int(11) NOT NULL DEFAULT '-1',
  `MoveNum` int(11) DEFAULT '-1',
  `CurrentPP` int(11) DEFAULT '-1',
  `MaxPP` int(11) DEFAULT '-1',
  PRIMARY KEY (`CharID`,`RecruitIndex`,`UsingTempStats`,`MoveSlot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recruit_volatile_status`
--

DROP TABLE IF EXISTS `recruit_volatile_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recruit_volatile_status` (
  `CharID` varchar(255) NOT NULL,
  `RecruitIndex` int(11) NOT NULL,
  `UsingTempStats` tinyint(1) NOT NULL,
  `StatusNum` int(11) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Emoticon` int(11) DEFAULT NULL,
  `Counter` int(11) DEFAULT NULL,
  `Tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`RecruitIndex`,`UsingTempStats`,`StatusNum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `script_extras_general`
--

DROP TABLE IF EXISTS `script_extras_general`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `script_extras_general` (
  `CharID` varchar(255) NOT NULL,
  `SpawnMap` varchar(255) DEFAULT NULL,
  `SpawnX` int(11) DEFAULT NULL,
  `SpawnY` int(11) DEFAULT NULL,
  `DungeonID` int(11) DEFAULT NULL,
  `DungeonX` int(11) DEFAULT NULL,
  `DungeonY` int(11) DEFAULT NULL,
  `TurnsTaken` int(11) DEFAULT NULL,
  `EvolutionActive` tinyint(1) DEFAULT NULL,
  `Permamuted` tinyint(1) DEFAULT NULL,
  `HousingCenterMap` varchar(255) DEFAULT NULL,
  `HousingCenterX` int(11) DEFAULT NULL,
  `HousingCenterY` int(11) DEFAULT NULL,
  `DungeonMaxX` int(11) DEFAULT NULL,
  `DungeonMaxY` int(11) DEFAULT NULL,
  `DungeonSeed` int(11) DEFAULT NULL,
  `PlazaEntranceMap` varchar(255) DEFAULT NULL,
  `PlazaEntranceX` int(11) DEFAULT NULL,
  `PlazaEntranceY` int(11) DEFAULT NULL,
  PRIMARY KEY (`CharID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `step_counter_trigger_event`
--

DROP TABLE IF EXISTS `step_counter_trigger_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `step_counter_trigger_event` (
  `CharID` varchar(255) NOT NULL,
  `ID` varchar(255) NOT NULL,
  `Steps` int(11) DEFAULT NULL,
  `StepsCounted` int(11) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stepped_on_tile_trigger_event`
--

DROP TABLE IF EXISTS `stepped_on_tile_trigger_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stepped_on_tile_trigger_event` (
  `CharID` varchar(255) NOT NULL,
  `ID` varchar(255) NOT NULL,
  `MapID` varchar(255) DEFAULT NULL,
  `X` int(11) DEFAULT NULL,
  `Y` int(11) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `story`
--

DROP TABLE IF EXISTS `story`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `story` (
  `CharID` varchar(255) NOT NULL,
  `CurrentChapter` varchar(255) NOT NULL,
  `CurrentSegment` int(10) NOT NULL,
  PRIMARY KEY (`CharID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `story_chapters`
--

DROP TABLE IF EXISTS `story_chapters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `story_chapters` (
  `CharID` varchar(255) NOT NULL,
  `Chapter` int(10) unsigned NOT NULL,
  `Complete` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`Chapter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `story_helper_state_settings`
--

DROP TABLE IF EXISTS `story_helper_state_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `story_helper_state_settings` (
  `CharID` varchar(255) NOT NULL,
  `SettingKey` varchar(255) NOT NULL,
  `Value` text,
  PRIMARY KEY (`CharID`,`SettingKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `CharID` varchar(255) NOT NULL,
  `Slot` int(11) NOT NULL,
  `RecruitIndex` int(11) DEFAULT NULL,
  `UsingTempStats` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`Slot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trigger_events`
--

DROP TABLE IF EXISTS `trigger_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trigger_events` (
  `CharID` varchar(255) NOT NULL,
  `Type` tinyint(4) NOT NULL,
  `ID` varchar(255) NOT NULL,
  `Action` int(11) DEFAULT NULL,
  `TriggerCommand` int(11) DEFAULT NULL,
  `AutoRemove` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`CharID`,`ID`,`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-04 23:47:19
