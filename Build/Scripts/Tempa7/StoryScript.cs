﻿using Server.Network;
using Server.Stories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Maps;

namespace Script
{
    public partial class Main
    {

        public static void BossBattleScripted(Client client, int param1, int param2)
        {
            switch (param1)
            {
                case 0://ninetails
                    {
                        InstancedMap map = new InstancedMap(MapManager.GenerateMapID("i"));
                        MapCloner.CloneMapTiles(MapManager.RetrieveMap(81), map);
                        map.MapBase = 81;
                        Messenger.PlayerWarp(client, map, 9, 14);
                        StoryManager.PlayStory(client, 6);
                    }
                    break;
            }
        }

    }
}
